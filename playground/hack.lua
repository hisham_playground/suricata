local ffi = require("ffi")
ffi.cdef[[
		int db_set(char*key, size_t *key_length, const char *value, size_t value_length);
	]]
local dblib = ffi.load("libhack.so", true)

function init (args)
    local needs = {}
    needs["packet"] = tostring(true)
	return needs
end

function match(args)
	local retvals= {}
	retvals["retval"] = 0
	local packet = args['packet']
	_, _, x, y, z = string.find(packet,"%s*HELLO_WORLD:%s*(%d+)%s*+%s*(%d+)%s*=%s*(%d+)%s*")
	if (x+y-z == 0) then
		print ("HELLO_WORLD detected")
		local p = string.gsub(packet, "HELLO_WORLD", "HELLO_HACKR")
		local buf = ffi.new("uint8_t[8]", 8)
                local buflen = ffi.new("size_t[1]", 1)
		local ret = dblib.db_set(buf, buflen, p, #p);
		retvals["z"] = z
		retvals["REPLACE_ID"] = ffi.string(buf, buflen[0])
		retvals["retval"] = 1 -- condition inverse à #4?
    end
	return retvals
end
return 0

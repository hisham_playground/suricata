#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <libmemcached/memcached.h>
#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

#define MY_DEST_MAC0	0x00
#define MY_DEST_MAC1	0x00
#define MY_DEST_MAC2	0x00
#define MY_DEST_MAC3	0x00
#define MY_DEST_MAC4	0x00
#define MY_DEST_MAC5	0x00

#define DEFAULT_IF	"wlan0"


static memcached_st *memc = NULL;
static int replace_id = 0;
static memcached_return rc;

static memcached_st *db_access_memc(memcached_st *memc)
{
	static memcached_st *m;
	if (memc) m = memc;

	return m;
}

static int db_init()
{
	memcached_server_st *servers = NULL;
	memc = memcached_create(NULL);
	db_access_memc(memc);

	servers= memcached_server_list_append(servers, "localhost", 11211, &rc);
	rc= memcached_server_push(memc, servers);

	if (rc != MEMCACHED_SUCCESS)
		fprintf(stderr,"Couldn't add server: %s\n",memcached_strerror(memc, rc));
	return rc;
}

int db_set(char*key, size_t *key_length, const char *value, size_t value_length)
{
	if (memc || MEMCACHED_SUCCESS == (rc = db_init())) {
		sprintf(key, "%.8x", replace_id);
		*key_length = 8;
		rc= memcached_set(memc, key, *key_length, value, value_length, (time_t)0, (uint32_t)0);

		if (rc == MEMCACHED_SUCCESS) {
			replace_id++;
		}
		else
			fprintf(stderr,"Couldn't store key: %s\n",memcached_strerror(memc, rc));
	}
	return (int)rc;
}

int db_get(char*key, size_t key_length, char **value, size_t *value_length)
{
	if (memc || MEMCACHED_SUCCESS == (rc = db_init())) {
		uint32_t flags;
		*value = memcached_get(memc, key, key_length, value_length, &flags, &rc);

		if (rc != MEMCACHED_SUCCESS)
			fprintf(stderr,"Couldn't store key: %s\n",memcached_strerror(memc, rc));
	}
	return (int)rc;
}


int send_replaced_packet(int tx_len, char *sendbuf)
{
	int sockfd;
	struct ifreq if_idx;
	struct ifreq if_mac;
	struct sockaddr_ll socket_address;
	char ifName[IFNAMSIZ];
	/* Get interface name */
	strcpy(ifName, DEFAULT_IF);

	/* Open RAW socket to send on */
	if ((sockfd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) == -1) {
		perror("socket");
	}

	/* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
		perror("SIOCGIFINDEX");
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
		perror("SIOCGIFHWADDR");

	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;

	socket_address.sll_protocol = htons(ETH_P_IP);
	/* Destination MAC */
	socket_address.sll_addr[0] = MY_DEST_MAC0;
	socket_address.sll_addr[1] = MY_DEST_MAC1;
	socket_address.sll_addr[2] = MY_DEST_MAC2;
	socket_address.sll_addr[3] = MY_DEST_MAC3;
	socket_address.sll_addr[4] = MY_DEST_MAC4;
	socket_address.sll_addr[5] = MY_DEST_MAC5;

	/* Send packet */
	if (sendto(sockfd, sendbuf, tx_len, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0)
		printf("Send failed\n");

	return 0;
}
